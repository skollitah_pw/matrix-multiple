package pl.edu.pw.okno;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
class Complex {
  final double real;
  final double imaginary;

  Complex add(Complex other) {
    double newReal = real + other.real;
    double newImaginary = imaginary + other.imaginary;

    return new Complex(real, imaginary);
  }

  Complex multiply(Complex other) {
    double newReal = real * other.real - imaginary * other.imaginary;
    double newImaginary = real * other.imaginary + imaginary * other.real;

    return new Complex(newReal, newImaginary);
  }
}
