package pl.edu.pw.okno;

import java.util.Date;

import static pl.edu.pw.okno.Preconditions.checkArgument;

public class MatrixMultiplier {

  public static void main(String[] args) {
    checkArgument(args.length == 1, "Invalid number of arguments");
    Integer size = Integer.parseInt(args[0]);

    Matrix m1 = Matrix.random(size);
    Matrix m2 = Matrix.random(size);

    Date start = new Date();
    m1.multiply(m2);
    Date end = new Date();

    long milis = end.getTime() - start.getTime();

    System.out.println("Multiplication time in milliseconds: " + milis);
  }
}
