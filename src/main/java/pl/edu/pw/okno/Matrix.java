package pl.edu.pw.okno;

import lombok.RequiredArgsConstructor;

import java.util.Random;

@RequiredArgsConstructor
class Matrix {

  final Complex[][] matrix;

  Matrix multiply(Matrix other) {
    Complex[][] newMatrix = new Complex[matrix.length][matrix.length];

    for (int i = 0; i < matrix.length; i++) {
      for (int j = 0; j < matrix.length; j++) {
        newMatrix[i][j] = calcCell(i, j, other);
      }
    }

    return new Matrix(newMatrix);
  }

  private Complex calcCell(int i, int j, Matrix other) {
    Complex sum = new Complex(0, 0);

    for (int k = 0; k < matrix.length; k++) {
      sum.add(matrix[i][k].multiply(other.matrix[k][j]));
    }

    return sum;
  }

  static Matrix random(int size) {
    Random r = new Random();
    double rangeMin = -100.0;
    double rangeMax = 100.0;
    Complex[][] newMatrix = new Complex[size][size];

    for (int i = 0; i < size; i++) {
      for (int j = 0; j < size; j++) {
        double real = rangeMin + (rangeMax - rangeMin) * r.nextDouble();
        double imaginary = rangeMin + (rangeMax - rangeMin) * r.nextDouble();
        newMatrix[i][j] = new Complex(real, imaginary);
      }
    }

    return new Matrix(newMatrix);
  }
}
