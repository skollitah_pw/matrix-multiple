Build
```sh
./gradlew fatJar
```

Run example
```sh
cd build/libs/
java -jar matrix-multiple-1.0-SNAPSHOT-all.jar 100
```